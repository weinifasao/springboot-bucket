## 集成https配置及http跳转https 

用jdk工具生成证书 keytool -genkey -alias tomcat -keyalg RSA -keystore ./server.keystore


## 许可证

Copyright (c) 2018 Xiong Neng

基于 MIT 协议发布: <http://www.opensource.org/licenses/MIT>
