package com.xncoding.jwt.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class MyController {
	
	@RequestMapping("")
	public String index(String username,HttpServletRequest request) {
		if(username == null) {
			throw new RuntimeException("请传入用户名");
		}
		request.getSession().setAttribute("username", username);
		return "index";
	}
	
	


}
